#!/bin/bash
VAGRANT_VERSION=1.8.1
VAGRANT_FILE=vagrant_${VAGRANT_VERSION}_x86_64.deb

echo "=== Installing wget === "
sudo apt-get install wget

echo "=== Downloading Vagrant === "
pushd .
cd /tmp
wget https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/${VAGRANT_FILE}

echo "=== Installing Vagrant === "
sudo dpkg -i /tmp/${VAGRANT_FILE}
popd

echo "=== Install vagrant r10k plugin === "
vagrant plugin install vagrant-r10k

# I don't think puppet is required to be installed locally
#echo "=== Installing Puppet === "
#sudo apt-get install puppet
