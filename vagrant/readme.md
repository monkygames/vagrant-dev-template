# Summary
This directory contains a vagrant environment for creating a development environment using Vagrant, Puppet, and r10k.

# Development
The configuration syncs the root project directory (host) to the /opt/project on the guest.  Feel free to change this to suite your projects needs.
This can be changed in the vagrant file under the comment 'sync folders'.  The syntax is:

```
  config.vm.synced_folder "HOST_SRC", "GUEST_DEST"
```

# Setup
Requires vagrant, vagrant_r10k, puppet, git, and ssh.

## Linux Host

This section describes the setup for a Linux Host.  This also assumes a debian based system. 

### Quick Install



### Manual Install
This section describes the necessary steps for installing the required software.

* Download Vagrant
  * Download the latest stable version from the [Vagrant Downloads](https://www.vagrantup.com/downloads.html).
* Install vagrant

```
sudo dpkg -i VAGRANT_FILE.deb
```

* Install r10k plugin for vagrant

```
vagrant plugin install vagrant-r10k
```

## Windows Host

TODO

# Deployment Configuration
Puppet is used for deploying and configuring software.

## r10k
r10K is a dependency management tool for puppet modules.  It uses the puppet/Puppetfile for configuration.
r10k is installed via a vagrant plugin.  This plugin has a limitation of only allowing git repositories in the Puppetfile.
This means that specifying modules from the PuppetForge is not allowed.

## Puppet
The puppet/manifests/box.pp is the puppet file to add your project specific puppet code for deploying and configurating software.
Its recommended that you don't build classes or defined types; meaning for the serious work, build a proper puppet module.
Use box.pp for defining your resources and using class that are pulled from r10k.

# Run

* Run vagrant
```
vagrant up
```

* The default IP is: 
(http://192.168.33.10)

* To connect to the vm via ssh
```
vagrant ssh
```