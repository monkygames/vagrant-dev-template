# Introduction
This repo is a template for creating projects in which vagrant is used for deploying a development environment.

# Project
This space should be reserved for documentation relating to the project.

# Vagrant
The vagrant directory contains the configuration for setting up the environment.  Please read vagrant/readme.md. 
